package com.heima.article;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * APP文章服务
 */
@SpringBootApplication
public class ArticleJarApplication {

    public static void main(String[] args) {
        SpringApplication.run(ArticleJarApplication.class,args);
    }
}
