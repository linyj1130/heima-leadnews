package com.heima.behavior;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * APP行为数据采集服务
 */
@SpringBootApplication
public class BehaviorJarApplication {

    public static void main(String[] args) {
        SpringApplication.run(BehaviorJarApplication.class,args);
    }
}
